#include <iostream>
#include <cmath>

enum class TDMAState {
    INIT,
    FORWARD,
    BACKWARD
};

inline TDMAState tridiagonal(TDMAState state, double *r, double *b, double *p, int incp, double *x, int incx) {
    static double q;
    switch (state) {
        case TDMAState::INIT:
            p[0] = -r[2] / r[1];
            x[0] =  b[0] / r[1];
            return TDMAState::FORWARD;
        case TDMAState::FORWARD:
            q = r[1] + r[0]*p[-incp];
            p[0] = -r[2] / q ;
            x[0] =  ( b[0] - r[0]*x[-incx] ) / q;
            return TDMAState::FORWARD;
        case TDMAState::BACKWARD:
            x[0] += x[incx]*p[0];
            return TDMAState::BACKWARD;
    }
    return TDMAState::INIT;
}

double* solve(int N) {
    double *u = new double[N*N];
    double h = 1.0 / (N-1);
    double h2 = h*h;
    double *pu = u, *pv = u + N-1;
    for (int i = 0; i < N; i++, pv += N) {
        pu[i] = 0.0;
        pu[N+i] = i*h2;
        pv[0] = std::sin(3*i*h) / 3;
    }
    
    double r[3];
    double *q = new double[N];
    double *p;
    double c, x, x2;
    pu = u+2*N;
    for (int i = 2; i < N; i++, pu += N) {        
        TDMAState state = TDMAState::INIT;
        x = h; x2 = x*x;
        p = pu + 1;
        r[1] = 1.0 + 2*x2;
        r[2] = -x2;
        c = 2*pu[1-N] - pu[1-2*N] + x2*pu[0];
        state = tridiagonal(state, r, &c, q, 1, p, 1);
        ++p; ++q;
        int j;
        for (j = 2; j < N-2; j++, p++, q++) {
            x = j*h; x2 = x*x;
            r[0] = -x2;
            r[1] = 1.0 + 2*x2;
            r[2] = -x2;
            c = 2*pu[j-N] - pu[j-2*N];
            tridiagonal(state, r, &c, q, 1, p, 1);
        }
        x = 1.0-h; x2 = x*x;
        r[0] = -x2;
        r[1] = 1.0 + 2*x2;
        r[2] = 0;
        c = 2*pu[j-N] - pu[j-2*N] + x2*pu[N-1];
        tridiagonal(state, r, &c, q, 1, p, 1);
        state = TDMAState::BACKWARD;
        p--, q--;
        for (int j = 1; j < N-2; j++, p--, q--) {
            tridiagonal(state, 0, 0, q, 1, p, 1);
        }
        q++;
    }
    delete[] q;
    
    return u;
}

#define GNUPLOT

int main(int argc, char **argv) {
    int N = 5;
    if (argc > 1) {
        N = std::stoi(argv[1]);
    }
    double *u = solve(N);
    double *pu = u;
    double h = 1.0 / (N-1);
    for (int i = 0; i < N; i++, pu += N) {
        for (int j = 0; j < N; j++) {
            #ifdef GNUPLOT
            std::cout << i*h << " " << j*h << " " << pu[j] << std::endl;
            #else 
            std::cout << pu[j] << " " << std::endl;
            #endif
        }
        std::cout << std::endl;        
    }
    delete[] u;
    return 0;
}