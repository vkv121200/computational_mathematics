#include <iostream>
#include <cmath>
#include <fstream>
#include <format>
#include <string>

double* mesh(int n)
{
	double* x = new double[n];

	for (int i = 0; i < n; i++)
	{
		x[i] = 1.0 / ((double)n - 1.0) * (double)i;
	}

	return x;
}

double* f1(int n, double* x, double* u)
{
	double h = x[1] - x[0];

	double* f = new double[n];

	for (int j = 0; j < n; j++)
	{
		f[j] = 0.0;

		for (int i = 1; i < n - 1; i += 2)
		{
			double dudt, f_left, f_center, f_right;

			if (j == 0)
				dudt = -2.0 * u[n * j + i] + 2.0 * u[n * (j + 1) + i];
			else if (j == n - 1)
				dudt = 2.0 * u[n * (j - 1) + i] - 2.0 * u[n * j + i];
			else
				dudt = u[n * (j - 1) + i] - 2.0 * u[n * j + i] + u[n * (j + 1) + i];

			dudt /= (h * h);

			if (i == 1)
				f_left = 0.0;
			else
				f_left = 1.0 / x[i - 1] / x[i - 1] * dudt + (u[n * j + i - 2] - 2.0 * u[n * j + i - 1] + u[n * j + i]) / h / h;

			f_center = 1.0 / x[i] / x[i] * dudt + (u[n * j + i - 1] - 2.0 * u[n * j + i] + u[n * j + i + 1]) / h / h;

			if (i == n - 2)
				f_right = 1.0 / x[i + 1] / x[i + 1] * dudt + (2.0 * u[n * j + i] - 2.0 * u[n * j + i + 1]) / h / h;
			else
				f_right = 1.0 / x[i + 1] / x[i + 1] * dudt + (u[n * j + i] - 2.0 * u[n * j + i + 1] + u[n * j + i + 2]) / h / h;

			f[j] += 0.5 * 2.0 * h / 6.0 * (f_left + 4.0 * f_center + f_right);
		}
	}

	return f;
}

double* f2(int n, double* x, double* u)
{
	double h = x[1] - x[0];

	double* f = new double[n];

	f[0] = 0.0;

	for (int j = 1; j < n; j++)
	{
		if (j == 1)
		{
			double f_left = (u[n * j + n - 1] - u[n * (j - 1) + n - 1]) / h * (u[n * j + n - 1] - u[n * (j - 1) + n - 2]) / h;
			double f_right = (u[n * (j + 1) + n - 1] - u[n * j + n - 1]) / h * (u[n * (j + 1) + n - 1] - u[n * j + n - 2]) / h;

			f[j] = f[j - 1] + (f_left + f_right) / 2.0 * h;
		}
		else if (j == n - 1)
		{
			double f_left = (u[n * (j - 1) + n - 1] - u[n * (j - 2) + n - 1]) / h * (u[n * (j - 1) + n - 1] - u[n * (j - 2) + n - 2]) / h;
			double f_right = (u[n * j + n - 1] - u[n * (j - 1) + n - 1]) / h * (u[n * j + n - 1] - u[n * (j - 1) + n - 2]) / h;

			f[j] = f[j - 1] + (f_left + f_right) / 2.0 * h;
		}
		else
		{
			double f_left = (u[n * (j - 1) + n - 1] - u[n * (j - 2) + n - 1]) / h * (u[n * (j - 1) + n - 1] - u[n * (j - 2) + n - 2]) / h;
			double f_center = (u[n * j + n - 1] - u[n * (j - 1) + n - 1]) / h * (u[n * j + n - 1] - u[n * (j - 1) + n - 2]) / h;
			double f_right = (u[n * (j + 1) + n - 1] - u[n * j + n - 1]) / h * (u[n * (j + 1) + n - 1] - u[n * j + n - 2]) / h;

			f[j] = f[j - 2] + 2.0 * h / 6.0 * (f_left + 4.0 * f_center + f_right);
		}
	}

	return f;
}

double* read_file(int n, const char* file_name)
{
	double* u = new double[n * n];

	std::ifstream infile(file_name);
	std::string line;

	for (int i = 0; i < n; i++)
	{
		std::getline(infile, line);

		for (int j = 0; j < n; j++)
		{
			std::string token = line.substr(0, line.find(' '));

			u[n * j + i] = atof(token.c_str());

			line.erase(0, line.find(' ') + 1);
		}
	}

	return u;
}

int main()
{
	int N[] = { 51, 101, 201 };

	double** u = new double* [3];
	double** x = new double* [3];
	double** vf1 = new double* [3];
	double** vf2 = new double* [3];

	for (int i = 0; i < 3; i++)
	{
		u[i] = read_file(N[i], std::format("mesh{}_{}.txt", N[i], N[i]).c_str());
		x[i] = mesh(N[i]);
		vf1[i] = f1(N[i], x[i], u[i]);
		vf2[i] = f2(N[i], x[i], u[i]);

		FILE* f;
		fopen_s(&f, std::format("f{}_{}.txt", N[i], N[i]).c_str(), "w");

		for (int j = 0; j < N[i]; j++)
		{
			if (j > 0)
				fprintf(f, "\n");

			fprintf(f, "%.10f %.10f %.10f", x[i][j], vf1[i][j], vf2[i][j]);
		}

		fclose(f);
	}

	system("pause");

	return 0;
}
