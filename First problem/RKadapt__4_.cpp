#include <cmath>
#include <format>

const double pi = 3.141592653589793;

struct Point
{
	double t = 0.0;
	double x = 0.0;
	double y = 0.0;

	Point() {}

	Point(double t, double x, double y) : t(t), x(x), y(y) {}

	std::string Print() const
	{
		return std::format("{:.10f}\t{:.10f}\t{:.10f}", t, x, y);
	}
};

double f1(double t, double x, double y)
{
	return y;
}

double f2(double t, double x, double y, double alpha)
{
	return alpha * cos(t) - pow(x, 3);
}

double rk_step(Point& result, Point& prev, double h, double alpha)
{
	double x0 = prev.x;
	double y0 = prev.y;
	double t0 = prev.t;

	double k1[2], k2[2], k3[2], k4[2], k5[2], k6[2];

	k1[0] = f1(t0, x0, y0) * h;
	k1[1] = f2(t0, x0, y0, alpha) * h;

	k2[0] = f1(t0 + 0.5 * h, x0 + 0.5 * k1[0], y0 + 0.5 * k1[1]) * h;
	k2[1] = f2(t0 + 0.5 * h, x0 + 0.5 * k1[0], y0 + 0.5 * k1[1], alpha) * h;

	k3[0] = f1(t0 + 0.5 * h, x0 + 0.25 * (k1[0] + k2[0]), y0 + 0.25 * (k1[1] + k2[1])) * h;
	k3[1] = f2(t0 + 0.5 * h, x0 + 0.25 * (k1[0] + k2[0]), y0 + 0.25 * (k1[1] + k2[1]), alpha) * h;

	k4[0] = f1(t0 + h, x0 + k3[0] * 2.0 - k2[0], y0 + k3[1] * 2.0 - k2[1]) * h;
	k4[1] = f2(t0 + h, x0 + k3[0] * 2.0 - k2[0], y0 + k3[1] * 2.0 - k2[1], alpha) * h;

	k5[0] = f1(t0 + h * 2.0 / 3.0,
		x0 + (k1[0] * 7.0 + k2[0] * 10.0 + k4[0]) / 27.0,
		y0 + (k1[1] * 7.0 + k2[1] * 10.0 + k4[1]) / 27.0) * h;
	k5[1] = f2(t0 + h * 2.0 / 3.0,
		x0 + (k1[0] * 7.0 + k2[0] * 10.0 + k4[0]) / 27.0,
		y0 + (k1[1] * 7.0 + k2[1] * 10.0 + k4[1]) / 27.0, alpha) * h;

	k6[0] = f1(t0 + h * 0.2,
		x0 + (k1[0] * 28.0 - k2[0] * 125.0 + k3[0] * 546.0 + k4[0] * 54.0 - k5[0] * 378.0) / 625.0,
		y0 + (k1[1] * 28.0 - k2[1] * 125.0 + k3[1] * 546.0 + k4[1] * 54.0 - k5[1] * 378.0) / 625.0) * h;
	k6[1] = f2(t0 + h * 0.2,
		x0 + (k1[0] * 28.0 - k2[0] * 125.0 + k3[0] * 546.0 + k4[0] * 54.0 - k5[0] * 378.0) / 625.0,
		y0 + (k1[1] * 28.0 - k2[1] * 125.0 + k3[1] * 546.0 + k4[1] * 54.0 - k5[1] * 378.0) / 625.0, alpha) * h;

	result.x = x0 + k1[0] / 24.0 + k4[0] * 5.0 / 48.0 + k5[0] * 27.0 / 56.0 + k6[0] * 125.0 / 336.0;
	result.y = y0 + k1[1] / 24.0 + k4[1] * 5.0 / 48.0 + k5[1] * 27.0 / 56.0 + k6[1] * 125.0 / 336.0;
	result.t = t0 + h;

	double gamma1 = -(42.0 * k1[0] + 224.0 * k3[0] + 21.0 * k4[0] - 162.0 * k5[0] - 125.0 * k6[0]) / 336.0;
	double gamma2 = -(42.0 * k1[1] + 224.0 * k3[1] + 21.0 * k4[1] - 162.0 * k5[1] - 125.0 * k6[1]) / 336.0;
	double error = sqrt(pow(gamma1, 2) + pow(gamma2, 2));

	return error;
}

double Algorithm_adaptive(int& n, Point& now, Point& zero, double tol, double h, double alpha, const char* output = nullptr)
{
	FILE* f = nullptr;

	if (output != nullptr)
	{
		fopen_s(&f, output, "w");
		fprintf_s(f, "%s", zero.Print().c_str());
	}

	double error_global = 0.0;
	Point old = zero;

	bool done = false;
	for (n = 0; !done; n++)
	{
		double error_local = rk_step(now, old, h, alpha);

		while (error_local > tol || error_local < 0.1 * tol)
		{
			h = 0.95 * h * pow(tol / (error_local + 0.001l * tol), 1.0 / 6.0);
			error_local = rk_step(now, old, h, alpha);
		}

		if (old.t + h > 2.0 * pi)
		{
			done = true;

			h = 2.0 * pi - old.t;
			error_local = rk_step(now, old, h, alpha);
		}

		double L = std::max((3.0 * pow(now.x, 2) - 1.0) / 2.0, (-3.0 * pow(now.x, 2) + 1.0) / 2.0);

		error_global += error_local * exp(L * h);

		old = now;

		if (output != nullptr)
			fprintf_s(f, "\n%s", now.Print().c_str());
	}

	if (output != nullptr)
		fclose(f);

	return error_global;
}

bool newton_method(Point& result, Point& zero, double tol, double h, double alpha)
{
	const double dx = 0.1;
	const double dy = 0.1;

	Point start_dx = zero;
	start_dx.x += dx;

	Point start_dy = zero;
	start_dy.y += dy;

	int foo;
	Point finish;
	Algorithm_adaptive(foo, finish, zero, tol, h, alpha);

	Point finish_dx;
	Algorithm_adaptive(foo, finish_dx, start_dx, tol, h, alpha);

	Point finish_dy;
	Algorithm_adaptive(foo, finish_dy, start_dy, tol, h, alpha);

	double w_0_0 = (finish_dx.x - finish.x) / dx - 1.0;
	double w_0_1 = (finish_dy.x - finish.x) / dy;
	double w_1_0 = (finish_dx.y - finish.y) / dx;
	double w_1_1 = (finish_dy.y - finish.y) / dy - 1.0;

	double det_w = w_0_0 * w_1_1 - w_0_1 * w_1_0;

	if (det_w > tol)
	{
		double w_obr_0_0 = w_1_1 / det_w;
		double w_obr_0_1 = -w_0_1 / det_w;
		double w_obr_1_0 = -w_1_0 / det_w;
		double w_obr_1_1 = w_0_0 / det_w;

		result.x = zero.x - dx * (w_obr_0_0 * (finish.x - zero.x) + w_obr_0_1 * (finish.y - zero.y));
		result.y = zero.y - dy * (w_obr_1_0 * (finish.x - zero.x) + w_obr_1_1 * (finish.y - zero.y));
		result.t = 0.0;

		return true;
	}
	else
	{
		printf("alpha = %.5f\ntol = %.10f\nDet = %.10f\n��������� ��������� �����\n\n", alpha, tol, det_w);

		return false;
	}
}

int poisk_tochki(Point& result, Point& start, double tol, double h, double alpha)
{
	Point old = start;
	Point now;

	if (!newton_method(now, old, tol, h, alpha))
		return -1;

	int n;
	for (n = 0;; n++)
	{
		int foo;
		Point check;
		Algorithm_adaptive(foo, check, now, tol, h, alpha);

		double error_x = fabs(now.x - check.x);
		double error_y = fabs(now.y - check.y);
		double error = sqrt(pow(error_x, 2) + pow(error_y, 2));

		if (error < tol)
			break;

		old = now;

		if (!newton_method(now, old, tol, h, alpha))
			return -1;
	}

	result = now;

	return n;
}

int main()
{
	setlocale(LC_ALL, "Russian");

	double h = 1.0;

	int n_tol = 3;
	double tol[] = { 1e-5, 1e-7, 1e-9 };

	int n_alpha = 2;
	double alpha[] = { 0.03, 0.3 };

	Point zero[2];
	zero[0] = Point(0.0, 0.0, 0.0);
	zero[1] = Point(0.0, 1.0, 0.2);

	Point init[6];
	Point finish[6];
	int n[6];
	double error_global[6];

	for (int j = 0; j < 2; j++)
		for (int i = 0; i < 3; i++)
		{
			int newton_iter = poisk_tochki(init[3 * j + i], zero[j], tol[i], h, alpha[j]);

			if (newton_iter < 0)
			{
				system("pause");

				return 1;
			}

			error_global[3 * j + i] = Algorithm_adaptive(n[3 * j + i], finish[3 * j + i], init[3 * j + i],
				tol[i], h, alpha[j], std::format("result {} {}.txt", j, i).c_str());
		}

	FILE* f;
	fopen_s(&f, "result.txt", "w");

	for (int j = 0; j < 2; j++)
	{
		for (int i = 0; i < 3; i++)
		{
			fprintf_s(f, "%d & %.10f & %.10f & %.10f & %.10f & %.10f\n", n[3 * j + i],
				init[3 * j + i].x, init[3 * j + i].y,
				finish[3 * j + i].x, finish[3 * j + i].y,
				error_global[3 * j + i]);
		}

		fprintf_s(f, "\n%.3f %.3f %.3f %.3f\n\n",
			(double)n[3 * j + 1] / (double)n[3 * j + 0],
			(double)n[3 * j + 2] / (double)n[3 * j + 1],
			error_global[3 * j + 0] / error_global[3 * j + 1],
			error_global[3 * j + 1] / error_global[3 * j + 2]);
	}

	fclose(f);

	system("pause");

	return 0;
}
